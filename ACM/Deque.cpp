//https://www.hackerrank.com/challenges/deque-stl/problem?fbclid=IwAR2KsvkBFNIrgk2-FGANzh43bS87wVrwkgnA1sKOEtBLI7ok5mNstMh0b1s
#include <bits/stdc++.h>
using namespace std;

#define fxdp fixed<<setprecision
#define ll long long
#define vi vector<int>
#define vvi vector<vector<int>>
#define pii pair<int, int>
#define pll pair<long, long>
#define mli map<long long, int>
#define msi map<string, int>
const int MAX = 1e5 +10;
#define FOR(i,a,n) for(int i = a; i<n; ++i)
#define SORT(a) sort(a.begin(), a.end())
void Check(vector<int> &a,int k){
    deque<int> dq;
    FOR(i,0,n){
        if(!dq.empty()&&dq.front()<=i-k){
            dq.pop_front();
        }
        while(!dq.empty()&& a[i]>=dq.front())
        {
            dq.pop_front();
        }
        dq.push_back();
    }
}
int main()
{
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);
    
    return 0;
}