//https://codeforces.com/contest/1526/problem/A
#include <bits/stdc++.h>
using namespace std;

#define fxdp fixed<<setprecision
#define ll long long
#define vi vector<int>
#define vvi vector<vector<int>>
#define pii pair<int, int>
#define pll pair<long, long>
#define mli map<long long, int>
#define msi map<string, int>
const int MAX = 1e5 +10;
#define FOR(i,a,n) for(int i = a; i<n; ++i)
#define SORT(a) sort(a.begin(), a.end())

int main()
{
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);
    int t;
    cin>>t;
    while(t--){
        int n;cin>>n;
        vi vec;
        vec.assign(0,2*n);
        for(int i=0;i<2*n;i++){
            cin>>vec[i];
        }
        FOR(i,0,vec.size()){
            if(i==2*n-2){
                if(vec[2*n-2]+vec[0]==vec[2*n-1]*2)
                    swap(vec[2*n-2],vec[2*n-1]);
            }
            else if(i ==2*n-1){
                if(vec[2*n-2]+vec[0]==vec[2*n-1]*2)
                    swap(vec[2*n-2],vec[2*n-1]);
            }
        }
    }
    return 0;
}