#include <bits/stdc++.h>
using namespace std;
int dp[10001][10001];

int lcs (vector<int> & a,vector<int> & b){
    for(int i=0;i<=a.size();i++){
        dp[i][0]=0;
    }
    for(int j=0;j<=b.size()+1;j++){
        dp[0][j]=0;
    }
    for(int i=1;i<=a.size();i++){
        for(int j=1;j<=b.size();j++){
            if(a[i-1]==b[j-1])
            {        
                    dp[i][j] = dp[i-1][j-1]+1;         
            }
            else{
                dp[i][j] = max(dp[i][j-1],dp[i-1][j]);
            }
        }
    }
    return dp[a.size()][b.size()];
}
int main(){
    int n,m;
    cin>>n>>m;
    vector<int> a;
    vector<int>b;
    for(int i=0;i<n;i++){
        int h;
        cin>>h;
        a.push_back(h);
    }
    for(int i=0;i<m;i++){
        int h;
        cin>>h;
        b.push_back(h);
    }
    cout<<lcs(a,b)<<endl;
    return 0;
}