//https://codeforces.com/contest/1525/problem/B
#include <bits/stdc++.h>
using namespace std;

#define fxdp fixed<<setprecision
#define ll long long
#define vi vector<int>
#define vvi vector<vector<int>>
#define pii pair<int, int>
#define pll pair<long, long>
#define mli map<long long, int>
#define msi map<string, int>
const int MAX = 1e5 +10;
#define for(i,a,n) for(int i = a; i<n; ++i)
#define sort(a) sort(a.begin(), a.end())
bool Check(vector<int> &a){
    for(i,0,a.size()){
        if(a[i]!=i+1)
            return false;
    }
    return true;
}
int main()
{
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);
    int t;
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        vector<int> vec;
        vec.assign(n,0);
        for(i,0,n){
            cin>>vec[i];
        }
        if(Check(vec)){
            cout<<0<<endl;
        }
        else if(vec[0]==n&&vec[n-1]==1){
            cout<<3<<endl;
        }
        else if(vec[0]==1||vec[n-1]==n){
            cout<<1<<endl;
        }
        else
            cout<<2<<endl;
    }
    return 0;
}