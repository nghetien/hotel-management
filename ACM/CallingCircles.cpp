#include <bits/stdc++.h>
using namespace std;

#define fxdp fixed<<setprecision
#define ll long long
#define vi vector<int>
#define vvi vector<vector<int>>
#define pii pair<int, int>
#define pll pair<long, long>
#define mli map<long long, int>
#define msi map<string, int>
const int MAX = 1e5 +10;

vector<vector<int>> graph;
vector<int> low,num;
vector<int> found;
stack<int> st;
vector<string> map2;
msi map1;
int n,m;
int counter;
int flag=1;
void dfs(int u){
    counter++;
    num[u]=low[u]= counter;
    st.push(u);
    for(int v:graph[u]){
        if(!found[v]){
            if(num[v]>0){
                low[u] = min(low[u],num[v]);
            }
            else{
                dfs(v);
                low[u] = min(low[u],low[v]);
            }
        }
    }
    if(num[u] == low[u]){
        int v;
        do{
            v = st.top();
            st.pop();
            if(u==v){
                cout<<map2[v-1]<<endl;
            }
            else
                cout<<map2[v-1]<<", ";
            found[v]=true;
        }while(v != u);
    }
}
void Tarjan(){
    counter =0;
    low.assign(n+1,0);
    num.assign(n+1,0);
    found.assign(n+1,false);
    counter = 0;
    st = stack<int>();
    for(int i=1; i<=n;i++){
        if(!num[i]){
            dfs(i);
        }
    }

}
int main()
{
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);
    int dem =1;
    while (1){
        dem = 1;
        map1.clear();
        map2.clear();
        graph.clear();
        cin>>n>>m;
        graph.assign(n + 1, vector<int>());
        for(int i=0;i<m;i++){
            string a,b;
            cin>>a>>b;
            if(!map1[a]){
                map1[a]=dem;
                map2.push_back(a);
                dem++;
            }
            if(!map1[b]){
                map1[b]=dem;
                map2.push_back(b);
                dem++;
            }
            graph[map1[a]].push_back(map1[b]);
        }
        if(n==0 && m==0)
            break;
        cout<<"Calling circles for data set "<<flag<<":\n";
        Tarjan();
        flag++;
    }
    return 0;
}
