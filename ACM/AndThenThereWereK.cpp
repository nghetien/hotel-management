//https://codeforces.com/contest/1527/problem/A
#include <bits/stdc++.h>
using namespace std;

#define fxdp fixed<<setprecision
#define ll long long
#define vi vector<int>
#define vvi vector<vector<int>>
#define pii pair<int, int>
#define pll pair<long, long>
#define mli map<long long, int>
#define msi map<string, int>
const int MAX = 1e5 +10;
#define FOR(i,a,n) for(int i = a; i<n; ++i)
#define SORT(a) sort(a.begin(), a.end())

int main()
{
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);
    int t;
    while(t--){
        ll int n;cin>>n;
        if()
        if(n==1){
            cout<<1<<endl;
        }
        else if(n==2)
            cout<<1<<endl;
        else if(n%2!=0){
            cout<<n-2<<endl;
        }
        else
            cout<<n-3<<endl;
    }
    
    return 0;
}