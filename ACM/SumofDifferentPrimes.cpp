#include <bits/stdc++.h>
using namespace std;

#define fxdp fixed<<setprecision
#define ll long long
#define vi vector<int>
#define vvi vector<vector<int>>
#define pii pair<int, int>
#define pll pair<long, long>
#define mli map<long long, int>
#define msi map<string, int>
const int MAX = 1e5 +10;
#define FOR(i,a,n) for(int i = a; i<n; ++i)
#define SORT(a) sort(a.begin(), a.end())
vi primes;
int dp[1122][15];

void check(){
    vi check(1120,1);
    check[0] = check[1] = 0;
    for(int i = 2; i<1120 + 1; ++i)
        if (check[i])
        {
            primes.push_back(i);
            for (int j = i*i; j<1120; j+=i)
                check[j] = 0;
        }
}
int main()
{
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);
    int n,k;
    check();
    dp[0][0] = 1 ;
    for(int i = 0 ; i < primes.size(); i++ ){
        for( int j = 1120 ; j >= primes[i] ; j-- )
            for(int  k = 1 ; k < 15 ; k++ ){
                dp[j][k] += dp[ j - primes[i]][k - 1];
        }
    }
    while(cin>>n>>k){
        if(n==0&&k==0){
            break;
        }
        cout<<dp[n][k]<<endl;
    }
    return 0;
}