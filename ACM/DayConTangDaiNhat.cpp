#include <bits/stdc++.h>
using namespace std;

#define fxdp fixed<<setprecision
#define ll long long
#define vi vector<int>
#define vvi vector<vector<int>>
#define pii pair<int, int>
#define pll pair<long, long>
#define mli map<long long, int>
#define msi map<string, int>
const int MAX = 1e5 +10;
vi pre;
vi P;
int len=0,end1=0;
int LIS(vector<int> &a,vector<int> &dp){
    int n= a.size();
    vector<int> lis;
    dp.assign(n,1);
    for(int j,i=0;i<n;i++){
        j= upper_bound(lis.begin(),lis.end(),a[i]-1)-lis.begin();
        dp[i] = j+1;
        if(j==lis.size()){
            lis.push_back(a[i]);
            pre.push_back(i);
        }
        else{
            lis[j]=a[i];
            pre[j] = i;
        }
        if(j!=0)
            P.push_back(pre[j-1]);
        else
            P.push_back(-1);
        if(j+1>=len) {
            len=j+1;
            end1=i;
        }  
    }
    return lis.size();
}
int main()
{
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);
    int t;
    cin >>t;
    cin.ignore();
    string s;
    getline(cin,s);
    while(t--){
        string s;
        getline(cin,s);
        vector<int> a;
        while(s.size() != 0)
        {
            a.push_back(stoi(s));
            getline(cin,s);
        }
        vector<int> dp;
        len=0;
        end1=0;
        pre.clear();
        P.clear();
        int pos = LIS(a,dp);
        vector<int> vec;
        while(1){
            vec.push_back(a[end1]);
            end1= P[end1];
            if(P[end1]<0){
                vec.push_back(a[end1]);
                break;
            }
        }
        cout<<"Max hits: "<<pos<<endl;
        for(int i=vec.size()-1;i>=0;i--){
            cout<<vec[i]<<endl;
        }
        if(t!=0)
            cout<<endl;
    }
    return 0;
}