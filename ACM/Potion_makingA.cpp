//https://codeforces.com/contest/1525/problem/A
#include <bits/stdc++.h>
using namespace std;

#define fxdp fixed<<setprecision
#define ll long long
#define vi vector<int>
#define vvi vector<vector<int>>
#define pii pair<int, int>
#define pll pair<long, long>
#define mli map<long long, int>
#define msi map<string, int>
const int MAX = 1e5 +10;
#define FOR(i,a,n) for(int i = a; i<n; ++i)
#define SORT(a) sort(a.begin(), a.end())
int GCD(int a,int b){
    while (a*b != 0){ 
        if (a > b){
            a %= b;
        }else{
            b %= a;
        }
    }
    return a + b;
}
int main(){
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);
    int t;
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        int x = GCD(100,n);
        if(x==1){
            cout<<100<<endl;
        }
        else{
            cout<<100/x<<endl;
        }
    }
    return 0;
}