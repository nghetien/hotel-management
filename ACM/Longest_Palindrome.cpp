#include <bits/stdc++.h>
using namespace std;

int LongestPalindrome(string &a,string &b) {
    vector<vector<int> > res;
    res.assign(a.size() + 1, vector<int>(b.size() + 1, 0));

    for (int i = 1; i <= a.size(); i++)
        for (int j = 1; j <= b.size(); j++)
            if (a[i - 1] == b[j - 1])
                res[i][j] = res[i-1][j-1] + 1;
            else
                res[i][j] = max(res[i-1][j], res[i][j-1]);

    return res[a.size()][b.size()];
}

int main() {
    int t;
    cin >> t;
    cin.ignore();
    while (t--) {
        string a, b;
        getline(cin,a);
        if (a=="") {
            cout<<0<<endl;
            continue;
        }
        b = a;
        for (int i = 0; i < b.size()/2; i++)
            swap(b[i], b[b.size() - i - 1]);
        cout << LongestPalindrome(a,b) << endl;
    }
    return 0;
}