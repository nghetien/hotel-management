#include <bits/stdc++.h>
using namespace std;

#define fxdp fixed<<setprecision
#define ll long long
#define vi vector<int>
#define vvi vector<vector<int>>
#define pii pair<int, int>
#define pll pair<long, long>
#define mli map<long long, int>
#define msi map<string, int>
const int MAX = 1e5 +10;

vector<int> graph[MAX];
vector<int> low,num;
vector<int> found;
stack<int> st;
int n,m;
int counter;
void dfs(int u){
    counter++;
    num[u]=low[u]= counter;
    st.push(u);
    for(int v:graph[u]){
        if(!found[v]){
            if(num[v]>0){
                low[u] = min(low[u],num[v]);
            }
            else{
                dfs(v);
                low[u] = min(low[u],low[v]);
            }
        }
    }
    if(num[u] == low[u]){
        cout<<"Found a SCC:";
        int v;
        do{
            v = st.top();
            st.pop();

            cout<<" "<<v;
            found[v]=true;
        }while(v != u);
        cout<<endl;
    }
}
void Tarjan(){
    counter =0;
    low.assign(n+1,0);
    num.assign(n+1,0);
    found.assign(n+1,false);
    counter = 0;
    st = stack<int>();
    for(int i=1; i<=n;i++){
        if(!num[i]){
            dfs(i);
        }
    }

}
int main()
{
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);
    cin>>n>>m;
    for(int u,v,i =0;i<m;i++){
        cin>>u>>v;
        graph[u].push_back(v);
    }
    Tarjan();
    return 0;
}
