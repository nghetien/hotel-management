#include <bits/stdc++.h>
using namespace std;

#define fxdp fixed<<setprecision
#define ll long long
#define vi vector<int>
#define vvi vector<vector<int>>
#define pii pair<int, int>
#define pll pair<long, long>
#define mli map<long long, int>
#define msi map<string, int>
const int MAX = 1e5 +10;
#define FOR(i,a,n) for(int i = a; i<n; ++i)
#define SORT(a) sort(a.begin(), a.end())

vector <int> weights, profit;
vector<vector<int> > dp;

int knapsack (int N, int W) {
    dp.assign(N + 1, vector<int> (W + 1, 0));
    for (int j = 0; j <= W; j++)
        dp[0][j] = 0;
    for (int i = 1; i <= N; i++) {
        for (int j = 0; j <= W; j++) {
            dp[i][j] = dp[i - 1][j];
            if (j >= weights[i - 1] && dp[i][j] < dp[i - 1][j - weights[i - 1]] + profit[i - 1]) {
                dp[i][j] = dp[i - 1][j - weights[i - 1]] + profit[i - 1];
            }
        }
    }

    return dp[N][W];
}

void trace(int N, int W) {
    int totalWeight = 0, totalProfit = 0;
    vector<int> ans;
    while (N > 0 && W > 0) {
        if (weights[N - 1] <= W && dp[N][W] == dp[N - 1][W - weights[N - 1]] + profit[N - 1]) {
            ans.push_back(profit[N - 1]);
            totalWeight += weights[N - 1];
            totalProfit += profit[N - 1];
            W -= weights[N - 1];
        }
        N--;
    }
    //for (int i = ans.size() - 1; i >= 0; --i)
       // cout << ans[i] << " ";
    cout << totalWeight << " "<< totalProfit << endl;
}

int main() {
    int n, w;
    while (1) {
        cin >> w>>n;
        weights.assign(n, 0);
        profit.assign(n, 0);
        vector<pii> vecp;
        dp.clear();
        for (int i = 0; i < n; i++) {
            int a,b;
            cin>>a>>b;
            vecp.push_back(make_pair(a,b));
        }
        sort(vecp.begin(),vecp.end());

        FOR(i,0,n){
            weights[n-i-1]=vecp[i].first;
            profit[n-i-1]= (vecp[i].second);
        }
        if(n==0&&w==0)
            break;

        knapsack(n, w);
        trace(n, w);
    }
    return 0;
}